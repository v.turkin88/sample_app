Rails.application.routes.draw do
  root 'static_pages#home'
  get 'static_pages/help'    => 'static_pages#help'
  get 'static_pages/home'    => 'static_pages#home'
  get 'static_pages/about'   => 'static_pages#about'
  get 'static_pages/contact' => 'static_pages#contact'
  get 'signup'  => 'users#new'
  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
  
  resources :users
end
